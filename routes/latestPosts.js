const rp = require('request-promise');
const cheerio = require('cheerio');
const { data } = require('cheerio/lib/api/attributes');
const url = "https://time.com";

module.exports = async (req, res, next) => {

    try {
        let result = [];
        await rp(url)
        .then(function(html){
            const $ = cheerio.load(html);
            let links = $('.dek > a').toString();
            links = links.split("<a href=");
            for(let i =0;i<links.length;i++){
                links[i] = links[i].split('>\n')[0];
                links[i] = links[i].replace(/"/g, '');
            }
            const titles = $('.no-eyebrow').toString();
            let arrayTitles = titles.split(`<h3 class="title no-eyebrow">`);
            for(let i =0;i<arrayTitles.length;i++){
                arrayTitles[i] = arrayTitles[i].split('</h3>')[0]
            }
            for(let i =1;i<arrayTitles.length;i++){
                let data = {
                    "name": arrayTitles[i],
                    "link":url+links[i]
                }
                result.push(data);
            }
            console.log(result);
            return result

        })
        .catch(function(err){
            //handle error
            console.log(err);
        });
        res.status(200).send({
			success: true,
			data: result
		});
    } catch(err){
        console.log(err);
		next(err);
    }

}